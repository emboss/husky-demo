import { stdout } from 'node:process'
import { snakeToCamel, camelToSnake } from './lib/strings.js'

stdout.write(`snake -> camel: snake_case => ${snakeToCamel('snake_case')}\n`)

stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)

const testFn = (s) => {
  return s + '-single-quotes'
}

stdout.write(testFn('Test with\n'))

const tooLong = () => {
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
  stdout.write(`camel -> snake: camelCase => ${camelToSnake('camelCase')}\n`)
}

tooLong()
