export const snakeToCamel = (s) => {
  return s
    .toLowerCase()
    .replace(/[-_][a-z]/g, (group) => group.slice(-1).toUpperCase())
}

export const camelToSnake = (s) => {
  return s.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`)
}
